from LSTMnetwork import *
from dataHolder import *
from findBestNumOfLSTM import *


############################# FOR USD_EURO
"""
data_USD_EURO = DataHolder()
data_USD_EURO.fillWithDataFromFile("data/USD_EURO.data")
data_USD_EURO = data_USD_EURO.getCopyWithLossesFromEnd(0.4)
data_USD_EURO_cropped = data_USD_EURO.getCopyWithLossesFromEnd(0.2)
data_USD_EURO.plotData()


minRss = FindBestNumOfLSTM(lstmFrom=5,lstmTo=25,numOfCycles=1)
minRss.find(testDataHolder=data_USD_EURO,trainDataHolder=data_USD_EURO_cropped,verbose=True)
minRss.plot()

lstm = LSTMnetwork(data_USD_EURO_cropped,minRss.getBestLSTMcount())
lstm.train(plotErrors=True,verboseTrain=True,maxError = 0.005)
lstm.plotResults(data_USD_EURO)
"""


############################# FOR SIN
"""
data_sin = DataHolder()
data_sin.fillWithSin(400,0)
data_sin_cropped = data_sin.getCopyWithLossesFromEnd(0.7)
data_sin.plotData()

minRss = FindBestNumOfLSTM(lstmFrom=1,lstmTo=12,numOfCycles=2)
minRss.find(testDataHolder=data_sin,trainDataHolder=data_sin_cropped,verbose=True)
minRss.plot()

lstm = LSTMnetwork(data_sin_cropped,minRss.getBestLSTMcount())
lstm.train(plotErrors=True,verboseTrain=True,maxError = 0.005)
lstm.plotResults(data_sin)
"""
