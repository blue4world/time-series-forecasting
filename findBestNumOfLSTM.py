from  LSTMnetwork import *
import numpy

class FindBestNumOfLSTM:

    def __init__(self,lstmFrom=1,lstmTo=5,numOfCycles=1):
        self.lstmFrom = lstmFrom
        self.lstmTo = lstmTo
        self.numOfCycles = numOfCycles
        self.minRss = 0
        self.minRssIndex = 0
        self.RSSs = []

    def plot(self):
        x = range(self.lstmFrom,self.lstmTo)
        plt.figure(1)
        plt.title("Best RSS: "+str(self.minRss)+" for "+str(self.minRssIndex)+" LSTM neurons")
        plt.plot(x,self.RSSs)
        plt.xlabel("Num of LSTM neurons")
        plt.ylabel("RSS error")
        plt.show()



    def find(self,testDataHolder,trainDataHolder,verbose = False):
        RSSs = np.zeros(abs(self.lstmTo-self.lstmFrom))
        for c in range(0,self.numOfCycles):
            indexRss = 0
            for i in range(self.lstmFrom,self.lstmTo,1):
                ltsm = LSTMnetwork(trainDataHolder,i)
                ltsm.train(plotErrors = False, verboseTrain = False)
                rss, diff = ltsm.calculateError(testDataHolder)
                if RSSs[indexRss] != 0:
                    RSSs[indexRss]= round(((RSSs[indexRss]+rss[0])/2.0),4)
                else:
                    RSSs[indexRss] = round(rss[0],4)
                if verbose:
                    print "Cycle: "+str(c)+", LSTM neurons: "+str(i)+", current RSS: "+str(rss[0])+", total RSS: "+str(RSSs[indexRss])
                indexRss += 1

        ### Rss
        self.minRss = RSSs.min()
        positionOfMinRSS = numpy.where(RSSs==self.minRss)
        self.minRssIndex = self.lstmFrom + positionOfMinRSS[0][0]
        self.RSSs = RSSs


    def getBestLSTMcount(self):
        return self.minRssIndex

    def getMinRSS(self):
        return self.minRss


