from numpy.distutils.system_info import numarray_info
from pybrain.datasets import SequentialDataSet
from pybrain.tools.shortcuts import buildNetwork
from itertools import cycle
from pybrain.supervised import RPropMinusTrainer
from pybrain.structure.modules import LSTMLayer,LinearLayer,SigmoidLayer,SoftmaxLayer
from sys import stdout
import sys
import matplotlib.pyplot as plt
import numpy as np


class LSTMnetwork:
    def __init__(self,dataHolder,numOfLSTMLayer = 10):
        self.net = buildNetwork(1, numOfLSTMLayer, 1, hiddenclass=LSTMLayer, outputbias=False, recurrent=True, outclass=SigmoidLayer, )
        self.dataHolder = dataHolder
        #self.trainNeuralNet(net,dataHolder.getDataSet())


    def train(self, plotErrors = False, verboseTrain = False, maxError = 0.01):
        trainer = RPropMinusTrainer(self.net, dataset=self.dataHolder.getDataSet(), verbose=verboseTrain)

        self.train_errors = []
        #epochs = 50
        #for i in xrange(epochs):
        trainError = sys.maxint
        while trainError > maxError:
            trainer.trainEpochs(2)
            trainError = trainer.testOnData()
            self.train_errors.append(trainError)

        if plotErrors:
            plt.plot(range(0, len(self.train_errors)), self.train_errors)
            plt.xlabel('epoch')
            plt.ylabel('error')
            plt.show()

        if verboseTrain:
            print "Total number of epoch until error "+str(maxError)+" was "+str(len(self.train_errors))


    def calculateError(self,dataHolder = None):
        if dataHolder == None:
            dataHolder = self.dataHolder
        rss = 0
        diff = 0
        for sample, target in dataHolder.getDataSet().getSequenceIterator(0):
            predictedSample = self.net.activate(sample)
            rss = rss + pow(predictedSample - target,2)
            diff = diff + abs(predictedSample - target)

        return rss, diff


    def plotResults(self, dataHolder = None, verbose = False):
        if dataHolder == None:
            dataHolder = self.dataHolder

        diff = []
        rss = []
        predicted = []
        predicted.append(0)
        output = ""
        for sample, target in dataHolder.getDataSet().getSequenceIterator(0):
            output += "sample = \t\t"+str(sample[0])+"\n"

            predictedSample = self.net.activate(sample)
            predicted.append(predictedSample)
            output += "predicted next sample = \t\t"+str(predictedSample[0])+"\n"

            output += "real next sample = \t\t"+str(target[0])+"\n"

            diffSample = abs(predictedSample-target)
            rssSample = pow(diffSample,2)
            diff.append(diffSample[0])
            rss.append(rssSample[0])
            output += "difference per sample = \t\t"+str(diffSample[0])+"\n"
            output += "RSS per sample = \t\t"+str(diffSample[0])+"\n"
            output += "------\n"

        if verbose:
            print output

        plt.figure(1)
        plt.subplot(411)

        plt.title("Real vs. predicted")
        plt.plot(dataHolder.getRawNormalizedData(),marker='o',color='g')
        plt.plot(predicted,marker='o',color='b')
        plt.legend(['Real', 'Predicted'], loc='upper right')

        plt.subplot(412)
        plt.title("Difference and RSS per sample")
        plt.plot(diff,marker='o',color='r')
        plt.plot(rss,marker='o',color='y')
        plt.legend(['Diff', 'RSS'], loc='upper right')


        rssSum = sum(rss)
        diffSum = sum(diff)
        ax = plt.subplot(413)
        plt.title("Total RSS and Difference")
        ax.bar([0.7], [rssSum],width=0.2,color='b',align='center')
        ax.bar([1.1], [diffSum],width=0.2,color='g',align='center')
        ax.set_xticks([])
        ax.legend(["RSS = "+str(round(rssSum,2)), "Diff = "+str(round(diffSum,2))], loc='upper right')

        ax = plt.subplot(414)
        plt.title("RSS and Diff Histogram")
        ax.hist(rss)
        ax.hist(diff)
        ax.legend(["RSS", 'Diff'], loc='upper right')

        plt.show()

