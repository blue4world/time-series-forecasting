import matplotlib.pyplot as plt
from sklearn.preprocessing import normalize
import numpy as np
from pybrain.datasets import SequentialDataSet
from itertools import cycle
import  random
import  copy

class DataHolder:
    def __init__(self, data = []):
        self.data = data

    def fillWithDataFromFile(self,filename):
        with open(filename, "r") as ins:
            for line in ins:
                self.data.append(float(line))

    def fillWithSin(self,numOfSamples,rndShift = 0):
        self.data = DataHolder.generateSin(numOfSamples,rndShift)

    def getCopyWithLossesFromStart(self,losses):
        numOfElements = int(round(len(self.data) * losses))
        #print "Odstanuji: "+str(numOfElements)+" z "+str(len(self.data))
        newData = copy.copy(self.data)
        for i in range(0,numOfElements):
            del(newData[0])

        return DataHolder(newData)

    def getCopyWithLossesFromEnd(self,losses):
        numOfElements = int(round(len(self.data) * losses))
        newData = copy.copy(self.data)
        for i in range(0,numOfElements):
            del(newData[len(newData)-1])

        return DataHolder(newData)

    def plotData(self,normalized = False):
        data = self.data
        if normalized:
            data = self.getRawNormalizedData()

        fig, ax=plt.subplots(1,1)
        ax.plot(data, label='data',marker='o')
        ax.grid()
        plt.show()

    def getRawData(self):
        return self.data

    def getDataSet(self):
        data = self.getRawNormalizedData()
        dataSet = SequentialDataSet(1, 1)

        for sample, next_sample in zip(data, cycle(data[1:])):
            dataSet.addSample(sample, next_sample)
        return dataSet

    def getRawNormalizedData(self):
        normalized = []
        for n in self.data:
            normalized.append( (n-min(self.data))/(max(self.data)-min(self.data)) )
        return normalized


    def __str__(self):
        string = ""
        for d in self.data:
            if not d == None:
                string += str(d)+"\n"

        string += "Total data count: "+str(len(self.data))+"\n"
        return  string


    @staticmethod
    def generateSin(numOfSamples,shift = 0):
        x = np.linspace(0, 4*np.pi, numOfSamples)
        y = np.sin(x)
        dataSin = []
        for d in y:
            d = (d + 1) / 2
            if shift != 0:
                d = d + random.uniform(-shift,shift)
            dataSin.append(d)

        return dataSin

